#include "mainwnd.h"

#include <QStatusBar>
#include <QVBoxLayout>
#include <QGroupBox>
#include <QTableWidget>
#include <QSettings>
#include <QPushButton>

#include "debugconsole.h"

#include "connectionsettings.h"

MainWindow::MainWindow(QWidget *parent)
  : QMainWindow(parent)
{
  setStatusBar(statusBar());
  auto centralWidget = new QWidget();
  auto centralLayout = new QVBoxLayout(centralWidget);

  auto connectionGroupBox = new QGroupBox(tr("Connection"), centralWidget);
  auto connectionGroupBoxLayout = new QHBoxLayout(connectionGroupBox);

  connectButton = new QPushButton(connectionGroupBox);
  settingsButton = new QPushButton(connectionGroupBox);
  connectionGroupBoxLayout->addWidget(connectButton);
  connectionGroupBoxLayout->addWidget(settingsButton);

  connect(settingsButton, SIGNAL(clicked()), this, SLOT(on_settings()));

  centralLayout->addWidget(connectionGroupBox);

  auto toolsTabs = new QTabWidget(centralWidget);

  // addTab() takes ownership of added/inserted widgets
  toolsTabs->addTab(new DebugConsole(), tr("Debug console"));

  centralLayout->addWidget(toolsTabs);

  // setCentralWidget takes ownership
  setCentralWidget(centralWidget);

  loadSettings();
}

MainWindow::~MainWindow()
{

}

void MainWindow::closeEvent(QCloseEvent *)
{
  saveSettings();
}

void MainWindow::saveSettings()
{
  // QSettings settings("btc64c.ini", QSettings::IniFormat);
  QSettings settings;

  bool maximized = isMaximized();

  settings.beginGroup("MainWindow");

  if (!maximized)
  {
    settings.setValue("size", size());
    settings.setValue("pos", pos());
  }

  settings.setValue("maximized", maximized);
  settings.endGroup();
}

void MainWindow::loadSettings()
{
  QSettings settings;

  settings.beginGroup("MainWindow");

  resize(settings.value("size", QSize(600, 400)).toSize());

  if (settings.contains("pos"))
    move(settings.value("pos").toPoint());

  if (settings.value("maximized", false).toBool())
    showMaximized();
  else
    showNormal();

  settings.endGroup();
}

void MainWindow::on_settings()
{
  ConnectionSettings dlg;
  dlg.exec();
}
