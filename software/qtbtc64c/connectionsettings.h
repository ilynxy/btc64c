#ifndef CONNECTIONSETTINGS_H
#define CONNECTIONSETTINGS_H

#include <QDialog>
#include "realcommode_settings.h"
#include "tcpclientmode_settings.h"
#include "tcpservermode_settings.h"

namespace Ui {
class ConnectionSettings;
}

class ConnectionSettings : public QDialog
{
  Q_OBJECT
  
public:
  explicit ConnectionSettings(QWidget *parent = 0);
  ~ConnectionSettings();

protected:
  RealCOMMode_settings*     RealCOMMode_tab;
  TCPClientMode_settings*   TCPClientMode_tab;

protected slots:
  // void connectionTypeChanged(bool);

  void on_rbRealCom_toggled(bool checked);
  void on_rbTCPClient_toggled(bool checked);
  void on_rbTCPServer_toggled(bool checked);

private:
  Ui::ConnectionSettings *ui;

};

#endif // CONNECTIONSETTINGS_H
