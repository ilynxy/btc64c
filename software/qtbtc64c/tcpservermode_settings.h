#ifndef TCPSERVERMODE_SETTINGS_H
#define TCPSERVERMODE_SETTINGS_H

#include <QDialog>
#include "TCPSettings.h"

namespace Ui {
class tcpservermode_settings;
}

class tcpservermode_settings : public QDialog
{
  Q_OBJECT
  
public:
  explicit tcpservermode_settings(TCPSettings &ts_, QWidget *parent = 0);
  ~tcpservermode_settings();
  
protected slots:
  void settings_accepted();

private:
  Ui::tcpservermode_settings *ui;

  TCPSettings& ts;
};

#endif // TCPSERVERMODE_SETTINGS_H
