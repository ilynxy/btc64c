#include <QApplication>
#include <QSettings>
#include <QFile>
#include <QTranslator>
#include <QTcpSocket>
#include <QFileInfo>

#include <QtSerialPort/QSerialPort>
#include <QtSerialPort/QSerialPortInfo>

#include "mainwindow.h"

int main(int argc, char *argv[])
{
  QApplication a(argc, argv);

  a.setOrganizationName("uhg350");
  QSettings::setDefaultFormat(QSettings::IniFormat);

  if (a.applicationName().isEmpty())
    a.setApplicationName(QFileInfo(QCoreApplication::applicationFilePath()).baseName());

  // Support for local setting file
  QString localSettingsFilePath(a.applicationName() + ".ini");
  if (QFile::exists(localSettingsFilePath))
  {
    a.setOrganizationName(".");
    QSettings::setPath(QSettings::IniFormat, QSettings::UserScope, ".");
  }

  // Translation support
  QString locale = QLocale::system().name();
  QTranslator translator;
  translator.load(a.applicationName() + "_" + locale);
  a.installTranslator(&translator);

  MainWindow w;
  w.show();

  return a.exec();
}
