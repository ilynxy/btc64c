#ifndef COMMANDLINEEDITOR_H
#define COMMANDLINEEDITOR_H

#include <QComboBox>

class CommandLineEditor : public QComboBox
{
  Q_OBJECT
public:
  explicit CommandLineEditor(QWidget *parent = 0);
  
signals:
  
public slots:
  
protected slots:
  void on_text_changed(const QString& s);

  void on_returnPressed();

signals:
  void sendData(const QByteArray& data);
};

#endif // COMMANDLINEEDITOR_H
