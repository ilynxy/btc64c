#ifndef DEBUGCONSOLE_H
#define DEBUGCONSOLE_H

#include <QWidget>
#include <QPlainTextEdit>
#include <QPushButton>
#include <QCheckBox>

#include "commandlineeditor.h"

class DebugConsole : public QWidget
{
  Q_OBJECT
public:
  explicit DebugConsole(QWidget *parent = 0);
  
  QPlainTextEdit *logView;
  CommandLineEditor *cmdLine;
  QPushButton *clrButton;
  QCheckBox   *cbEnable;

signals:
  
public slots:
  
  void clearLogEvent();
};

#endif // DEBUGCONSOLE_H
