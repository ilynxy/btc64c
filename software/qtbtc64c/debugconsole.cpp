#include "debugconsole.h"

#include <QVBoxLayout>
#include <QListView>

DebugConsole::DebugConsole(QWidget *parent) :
  QWidget(parent)
{
  auto centralLayout = new QVBoxLayout(this);

  logView = new QPlainTextEdit(this);
  logView->setReadOnly(true);

  centralLayout->addWidget(logView);

  auto cmdLayout = new QHBoxLayout();
  centralLayout->addLayout(cmdLayout);

  cmdLine = new CommandLineEditor(this);
  cmdLine->setEditable(true);
  cmdLayout->addWidget(cmdLine);

  clrButton = new QPushButton("Clear log", this);
  clrButton->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Maximum);
  cmdLayout->addWidget(clrButton);

  cbEnable = new QCheckBox("Log", this);
  cbEnable->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Maximum);
  cbEnable->setChecked(true);
  cmdLayout->addWidget(cbEnable);

  connect(clrButton, SIGNAL(clicked()), this, SLOT(clearLogEvent()));

  setLayout(centralLayout);
}

void DebugConsole::clearLogEvent()
{
  logView->clear();
}
