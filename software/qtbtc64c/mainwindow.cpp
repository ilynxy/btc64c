#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QMessageBox>
#include <QDateTime>

// #include "connectionsettings.h"

#include "realcommode_settings.h"
#include "tcpservermode_settings.h"

#include "modulesdataview.h"

MainWindow::MainWindow(QWidget *parent) :
  QMainWindow(parent),
  ui(new Ui::MainWindow)
{
  ui->setupUi(this);

  dc = new DebugConsole();
  logView = dc->logView;
  connect(dc->cmdLine, SIGNAL(sendData(QByteArray)), this, SLOT(sendData(QByteArray)));

  ui->toolsTabs->addTab(dc, tr("Debug console"));

  dataView = new modulesdataview();
  ui->toolsTabs->addTab(dataView, tr("Data view"));

  connect(&sp, SIGNAL(readyRead()),          this, SLOT(readyRead()));
  connect(&sp, SIGNAL(bytesWritten(qint64)), this, SLOT(bytesWritten(qint64)));

  connect(&ss, SIGNAL(readyRead()),          this, SLOT(readyRead()));
  connect(&ss, SIGNAL(bytesWritten(qint64)), this, SLOT(bytesWritten(qint64)));

  connect(&ss, SIGNAL(connected()),          this, SLOT(connected()));
  connect(&ss, SIGNAL(disconnected()),       this, SLOT(disconnected()));
  connect(&ss, SIGNAL(error(QAbstractSocket::SocketError)),
          this, SLOT(socket_error(QAbstractSocket::SocketError)));


  loadSettings();

  updateConnectionCB();

  state = stDisconnected;

  QFont f = logView->font();
  f.setFamily("Consolas");
  logView->setFont(f);
}

MainWindow::~MainWindow()
{
  delete ui;
}

void MainWindow::closeEvent(QCloseEvent *)
{
  saveSettings();
}

void MainWindow::saveSettings()
{
  // QSettings settings("btc64c.ini", QSettings::IniFormat);
  QSettings settings;

  bool maximized = isMaximized();

  settings.beginGroup("MainWindow");

  if (!maximized)
  {
    settings.setValue("size", size());
    settings.setValue("pos", pos());
  }

  settings.setValue("maximized", maximized);
  settings.endGroup();

  //
  settings.beginGroup("RealCOM");

  settings.setValue("port", comParams.portName);

  settings.endGroup();

  //
  settings.beginGroup("TCPServer");

  settings.setValue("host", tcpParams.hostName);
  settings.setValue("port", tcpParams.port);

  settings.endGroup();

}

void MainWindow::loadSettings()
{
  QSettings settings;

  settings.beginGroup("MainWindow");

  resize(settings.value("size", QSize(600, 400)).toSize());

  if (settings.contains("pos"))
    move(settings.value("pos").toPoint());

  if (settings.value("maximized", false).toBool())
    showMaximized();
  else
    showNormal();

  settings.endGroup();

  //
  settings.beginGroup("RealCOM");

  comParams.portName = settings.value("port", "COM1").toString();

  settings.endGroup();

  //
  settings.beginGroup("TCPServer");

  tcpParams.hostName = settings.value("host", "192.168.0.206").toString();
  tcpParams.port     = settings.value("port", 4001).toUInt();

  settings.endGroup();
}

void MainWindow::open_channel()
{
  bool success = false;

  if (ui->connectionTypeComboBox->currentIndex() == 0)
  {
    QString errorString;
    sp.setPortName(comParams.portName);

    errorString = tr("open: ");
    success = sp.open(QIODevice::ReadWrite);

    if (success)
    {
      errorString = tr("setBaudRate: ");
      success = sp.setBaudRate(comParams.baudRate);
    }

    if (success)
    {
      errorString = tr("setDataBits: ");
      success = sp.setDataBits(comParams.dataBits);
    }

    if (success)
    {
      errorString = tr("setParity: ");
      success = sp.setParity(comParams.parity);
    }

    if (success)
    {
      errorString = tr("setStopBits: ");
      success = sp.setStopBits(comParams.stopBits);
    }

    if (success)
    {
      errorString = tr("setFlowControl");
      success = sp.setFlowControl(comParams.flowControl);
    }

    if (!success)
    {
      errorString += sp.errorString();
      error(errorString);
    }
    else
    {
      connected();
    }
  }
  else
  {
    ss.connectToHost(tcpParams.hostName, tcpParams.port);
    success = true;
  }
}

void MainWindow::abort_channel()
{
  if (sp.isOpen())
    sp.close();

  if (ss.isOpen())
    ss.abort();
}

void MainWindow::disconnect_channel()
{
  if (sp.isOpen())
  {
    sp.close();
    disconnected();
  }

  if (ss.isOpen())
  {
    ss.disconnectFromHost();
  }
}

void MainWindow::on_connectButton_clicked()
{
  // logView->appendPlainText("<b>test</b> <i>test</i>");

  if (state == stDisconnected)
  {
    state = stConnecting;

    ui->connectButton->setChecked(true);
    ui->connectButton->setText(tr("Connecting..."));

    ui->settingsButton->setEnabled(false);
    ui->connectionTypeComboBox->setEnabled(false);

    open_channel();
  }
  else
  if (state == stConnecting || state == stDisconnecting)
  {
    abort_channel();

    state = stDisconnected;
    ui->connectButton->setChecked(false);
    ui->connectButton->setText(tr("Connect"));

    ui->settingsButton->setEnabled(true);
    ui->connectionTypeComboBox->setEnabled(true);
  }
  else
  if (state == stConnected)
  {
    ui->connectButton->setChecked(true);
    ui->connectButton->setText(tr("Disconnecting..."));

    disconnect_channel();
  }
}

void MainWindow::updateConnectionCB()
{  
  QString s;
  s = QString(tr("RealCOM: %1 baud=%2 bits=%3 parity=%4 stop=%5 flow=%6"))
      .arg(
        comParams.portName,
        QString::number(comParams.baudRate),
        QString::number(comParams.dataBits),
        QString::number(comParams.parity),
        QString::number(comParams.stopBits),
        QString::number(comParams.flowControl)
      );

  if (ui->connectionTypeComboBox->count() < 1)
    ui->connectionTypeComboBox->insertItem(0, s);
  else
    ui->connectionTypeComboBox->setItemText(0, s);

  s = QString(tr("TCP Server: %1:%2"))
      .arg(
        tcpParams.hostName,
        QString::number(tcpParams.port)
      );
  if (ui->connectionTypeComboBox->count() < 2)
    ui->connectionTypeComboBox->insertItem(1, s);
  else
    ui->connectionTypeComboBox->setItemText(1, s);

}

void MainWindow::on_settingsButton_clicked()
{
  if (ui->connectionTypeComboBox->currentIndex() == 0)
  {
    RealCOMMode_settings dlg(comParams);
    dlg.exec();
  }
  else
  {
    tcpservermode_settings dlg(tcpParams);
    dlg.exec();
  }

  updateConnectionCB();
}

void MainWindow::readyRead()
{
  QByteArray data;
  if (sp.isOpen())
    data = sp.readAll();

  if (ss.isOpen())
    data = ss.readAll();

  qint64 size = data.size();

  QFile a("dump.rs");
  a.open(QIODevice::ReadWrite | QIODevice::Append);
  a.write(data);
  a.close();

  dataView->parseData(data);

  if (dc->cbEnable->isChecked())
  {
    QString s, v;
    s += QDateTime::currentDateTime().toString("hh:mm:ss.zzz ");
    s += "&lt;- <font color=\"#FF1010\">";

    for (int i = 0; i < size; i ++)
    {
      if (i != 0)
        s += QChar(' ');
      unsigned int x = static_cast<unsigned char>(data[i]);
      s += v.sprintf("%02X", x);
    }
    s += "</font>";
    logView->appendHtml(s);
  }
}

void MainWindow::bytesWritten(qint64 bytes)
{

}

void MainWindow::sendData(const QByteArray& data)
{
  if (sp.isOpen())
    sp.write(data);

  if (ss.isOpen())
    ss.write(data);

  QString s, v;
  s += QDateTime::currentDateTime().toString("hh:mm:ss.zzz ");
  s += "-&gt; <font color=\"blue\">";

  for (int i = 0; i < data.size(); i ++)
  {
    if (i != 0)
      s += QChar(' ');
    s += v.sprintf("%02X", static_cast<unsigned char>(data[i]));
  }
  s += "</font>";

  logView->appendHtml(s);
}

void MainWindow::connected()
{
  state = stConnected;
  ui->connectButton->setChecked(true);
  ui->connectButton->setText(tr("Disconnect"));

  ui->settingsButton->setEnabled(false);
  ui->connectionTypeComboBox->setEnabled(false);
}

void MainWindow::disconnected()
{
  state = stDisconnected;
  ui->connectButton->setChecked(false);
  ui->connectButton->setText(tr("Connect"));

  ui->settingsButton->setEnabled(true);
  ui->connectionTypeComboBox->setEnabled(true);
}

void MainWindow::socket_error(QAbstractSocket::SocketError)
{
  error(tr("socket: ") + ss.errorString());
}

void MainWindow::error(QString error)
{
  abort_channel();

  state = stDisconnected;
  ui->connectButton->setChecked(false);
  ui->connectButton->setText(tr("Connect"));

  ui->settingsButton->setEnabled(true);
  ui->connectionTypeComboBox->setEnabled(true);

  QMessageBox::critical(this, tr("Error!"), error);
}
