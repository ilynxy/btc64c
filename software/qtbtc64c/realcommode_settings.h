#ifndef REALCOMMODE_SETTINGS_H
#define REALCOMMODE_SETTINGS_H

#include <QWidget>
#include <QDialog>
#include <QComboBox>

#include "COMSettings.h"

namespace Ui {
class RealCOMMode_settings;
}

class RealCOMMode_settings : public QDialog
{
  Q_OBJECT
  
public:
  explicit RealCOMMode_settings(COMSettings& cs, QWidget *parent = 0);
  ~RealCOMMode_settings();

protected:
  void createUISettings();

  void loadSettings();
  void saveSettings();

  COMSettings&  comParams;

  void selectFoundData(QComboBox *cb, const QVariant &data);

  template<typename T>
  void storeSelectedData(QComboBox *cb, T &v);

private slots:
   void settings_accepted();

private:
  Ui::RealCOMMode_settings *ui;
};

#endif // REALCOMMODE_SETTINGS_H
