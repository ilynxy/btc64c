#include "tcpclientmode_settings.h"
#include "ui_tcpclientmode_settings.h"

TCPClientMode_settings::TCPClientMode_settings(QWidget *parent) :
  QWidget(parent),
  ui(new Ui::TCPClientMode_settings)
{
  ui->setupUi(this);
}

TCPClientMode_settings::~TCPClientMode_settings()
{
  delete ui;
}
