#include "connectionsettings.h"
#include "ui_connectionsettings.h"

ConnectionSettings::ConnectionSettings(QWidget *parent) :
  QDialog(parent),
  ui(new Ui::ConnectionSettings)
{
  ui->setupUi(this);

//  RealCOMMode_tab = new RealCOMMode_settings(this);
//  ui->verticalLayout->insertWidget(1, RealCOMMode_tab);
//  RealCOMMode_tab->setVisible(false);

  TCPClientMode_tab = new TCPClientMode_settings(this);
  ui->verticalLayout->insertWidget(2, TCPClientMode_tab);
  TCPClientMode_tab->setVisible(false);

  ui->rbRealCom->setChecked(true);

  setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);
}

ConnectionSettings::~ConnectionSettings()
{
  delete ui;
}

void ConnectionSettings::on_rbRealCom_toggled(bool checked)
{
//  RealCOMMode_tab->setVisible(checked);
}

void ConnectionSettings::on_rbTCPClient_toggled(bool checked)
{
  TCPClientMode_tab->setVisible(checked);
}

void ConnectionSettings::on_rbTCPServer_toggled(bool checked)
{

}
