#include "realcommode_settings.h"
#include "ui_realcommode_settings.h"

#include <QtSerialPort/QSerialPort>
#include <QtSerialPort/QSerialPortInfo>
#include <QLineEdit>


RealCOMMode_settings::RealCOMMode_settings(COMSettings &cs, QWidget *parent) :
  QDialog(parent),
  ui(new Ui::RealCOMMode_settings),
  comParams(cs)
{
  ui->setupUi(this);

  connect(this, SIGNAL(accepted()), this, SLOT(settings_accepted()));

  createUISettings();
}

RealCOMMode_settings::~RealCOMMode_settings()
{
  delete ui;
}

void RealCOMMode_settings::selectFoundData(QComboBox *cb, const QVariant &data)
{
  cb->setCurrentIndex(cb->findData(data));
}

void RealCOMMode_settings::createUISettings()
{
  // Port name handling
  foreach (const QSerialPortInfo& portInfo, QSerialPortInfo::availablePorts())
    ui->cbPortName->addItem(portInfo.portName());

  int index;
  index = ui->cbPortName->findText(comParams.portName);
  if (index == -1)
  {
    ui->cbPortName->insertItem(0, comParams.portName);
    index = 0;
  }

  ui->cbPortName->setCurrentIndex(index);

  // Baud rate
  const qint32 baudRates[] =
  {
    110, 300, 600, 1200, 2400, 4800,
    9600, 14400, 19200, 38400, 56000,
    57600, 115200, 128000, 230400, 256000,
    460800, 500000, 576000, 921600
  };

  for (size_t i = 0; i < sizeof(baudRates) / sizeof(baudRates[0]); ++ i)
    ui->cbBitPerSecond->addItem(QString::number(baudRates[i]), baudRates[i]);

  selectFoundData(ui->cbBitPerSecond, comParams.baudRate);

  auto lineEdit = ui->cbBitPerSecond->lineEdit();
  if (lineEdit != nullptr)
    lineEdit->setValidator(new QIntValidator(50, 4000000, this));

  // Data bits
  ui->cbDataBits->addItem("5", QSerialPort::Data5);
  ui->cbDataBits->addItem("6", QSerialPort::Data6);
  ui->cbDataBits->addItem("7", QSerialPort::Data7);
  ui->cbDataBits->addItem("8", QSerialPort::Data8);

  selectFoundData(ui->cbDataBits, comParams.dataBits);

  // Parity
  ui->cbParity->addItem(tr("None"),  QSerialPort::NoParity);
  ui->cbParity->addItem(tr("Even"),  QSerialPort::EvenParity);
  ui->cbParity->addItem(tr("Odd"),   QSerialPort::OddParity);
  ui->cbParity->addItem(tr("Space"), QSerialPort::SpaceParity);
  ui->cbParity->addItem(tr("Mark"),  QSerialPort::MarkParity);

  selectFoundData(ui->cbParity, comParams.parity);

  // Stop bits
  ui->cbStopBits->addItem(tr("1"),   QSerialPort::OneStop);
  ui->cbStopBits->addItem(tr("1.5"), QSerialPort::OneAndHalfStop);
  ui->cbStopBits->addItem(tr("2"),   QSerialPort::TwoStop);

  selectFoundData(ui->cbStopBits, comParams.stopBits);

  // Flow control
  ui->cbFlowControl->addItem(tr("None"), QSerialPort::NoFlowControl);
  ui->cbFlowControl->addItem(tr("Hardware (RTS/CTS)"), QSerialPort::HardwareControl);
  ui->cbFlowControl->addItem(tr("Software (XON/XOFF)"), QSerialPort::SoftwareControl);

  selectFoundData(ui->cbFlowControl, comParams.flowControl);
}

void RealCOMMode_settings::loadSettings()
{

}

void RealCOMMode_settings::saveSettings()
{

}

template<typename T>
void RealCOMMode_settings::storeSelectedData(QComboBox *cb, T &v)
{
  int index;
  index = cb->currentIndex();
  QVariant vv = cb->itemData(index);

  if (index != -1)
    v = static_cast<T>(vv.value<int>());

}


void RealCOMMode_settings::settings_accepted()
{
  comParams.portName = ui->cbPortName->currentText();

  storeSelectedData(ui->cbBitPerSecond, comParams.baudRate);
  storeSelectedData(ui->cbDataBits, comParams.dataBits);
  storeSelectedData(ui->cbParity, comParams.parity);
  storeSelectedData(ui->cbStopBits, comParams.stopBits);
  storeSelectedData(ui->cbFlowControl, comParams.flowControl);

}

