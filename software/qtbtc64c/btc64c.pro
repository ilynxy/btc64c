QT  += core gui network

greaterThan(QT_MAJOR_VERSION, 4) {
  QT      += widgets serialport
} else {
  CONFIG  += serialport
}

TARGET = btc64c

TEMPLATE = app

PRECOMPILED_HEADER = pch.h

HEADERS += \
    pch.h \
    debugconsole.h \
    commandlineeditor.h \
    realcommode_settings.h \
    mainwindow.h \
    COMSettings.h \
    modulesdataview.h \
    dataviewmodel.h \
    TCPSettings.h \
    tcpservermode_settings.h

SOURCES += \
    btc64c.cpp \
    debugconsole.cpp \
    commandlineeditor.cpp \
    realcommode_settings.cpp \
    mainwindow.cpp \
    modulesdataview.cpp \
    dataviewmodel.cpp \
    tcpservermode_settings.cpp

RESOURCES += \
    btc64c.qrc

win32:RC_FILE = btc64c.rc

CODECFORTR = UTF-8
TRANSLATIONS += \
    btc64c.ts

include(../dir.platform.pri)

FORMS += \
    realcommode_settings.ui \
    mainwindow.ui \
    modulesdataview.ui \
    tcpservermode_settings.ui
