#include "dataviewmodel.h"

dataviewmodel::dataviewmodel(QObject* parent) :
    QAbstractTableModel(parent)
{
    headerNames.clear();
    headerNames.append(tr("Module #"));
    for (size_t i = 0; i < 8; ++ i)
      headerNames.append(QString("Ch. %1").arg(i + 1));
}

dataviewmodel::~dataviewmodel()
{
}
