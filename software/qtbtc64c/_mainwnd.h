#ifndef BTC64C_MAINWND_H
#define BTC64C_MAINWND_H

#include <QMainWindow>
#include <QPushButton>

class MainWindow : public QMainWindow
{
  Q_OBJECT
public:
  explicit MainWindow(QWidget* parent = nullptr);
  ~MainWindow();

protected:
  void loadSettings();
  void saveSettings();

  void closeEvent(QCloseEvent *);

  QPushButton* connectButton;
  QPushButton* settingsButton;

protected slots:
  void on_settings();
};

#endif // BTC64C_MAINWND_H
