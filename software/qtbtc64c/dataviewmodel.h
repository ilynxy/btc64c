#ifndef DATAVIEWMODEL_H
#define DATAVIEWMODEL_H

#include <QAbstractTableModel>
#include <QStringList>
#include <QLocale>
#include <QList>

class dataviewmodel : public QAbstractTableModel
{
  Q_OBJECT

public:
  typedef struct
  {
    int     channel;
    qint16 v[8];
  } channelData;

protected:
  QStringList headerNames;
  QVector<channelData>  datamap;

public:
  dataviewmodel(QObject* parent = 0);
  ~dataviewmodel();

  int rowCount(const QModelIndex &parent) const
  {
    if (parent.isValid())
      return 0;

    return datamap.size();
  }

  int columnCount(const QModelIndex &parent) const
  {
    if (parent.isValid())
      return 0;

    return headerNames.size();
  }

  Qt::ItemFlags flags(const QModelIndex &index) const
  {
    if (index.column() == 0)
      return Qt::ItemIsEnabled | Qt::ItemIsSelectable; // | Qt::ItemIsUserCheckable | Qt::ItemIsEditable;
    else
      return Qt::ItemIsEnabled | Qt::ItemIsSelectable;
  }

  QVariant data(const QModelIndex &index, int role) const
  {
    if (role == Qt::DisplayRole)
    {
      QString value;

      const channelData& r = datamap.at(index.row());

      if (index.column() == 0)
        value = QString("%1").arg(r.channel);
      else
        value = QString("%1").arg(r.v[index.column() - 1]);

      return value;
    }

    if (role == Qt::TextAlignmentRole)
      return Qt::AlignCenter;

    return QVariant::Invalid;
  }

  QVariant headerData(int section, Qt::Orientation orientation, int role) const
  {
    Q_UNUSED(orientation);

    if (role == Qt::DisplayRole)
      return headerNames.at(section);

    if (role == Qt::TextAlignmentRole)
      return Qt::AlignCenter;

    return QVariant::Invalid;
  }


  void setChannelData(const channelData& data)
  {
    int i;
    bool replace = false;

    for (i = 0; i < datamap.size(); ++ i)
    {
      if (datamap.at(i).channel >= data.channel)
      {
        replace = (datamap.at(i).channel == data.channel);
        break;
      }
    }

    if (replace)
    {
      datamap.replace(i, data);
      emit dataChanged(createIndex(i, 1), createIndex(i, 8));
    }
    else
    {
      QModelIndex parent;
      beginInsertRows(parent, i, i);
      datamap.insert(i, data);
      endInsertRows();
    }
  }

  void resetChannelData()
  {
    beginResetModel();
    datamap.clear();
    endResetModel();
  }

  void updateChannelData()
  {
    beginResetModel();
    endResetModel();
  }
};

#endif // DATAVIEWMODEL_H
