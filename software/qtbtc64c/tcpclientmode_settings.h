#ifndef TCPCLIENTMODE_SETTINGS_H
#define TCPCLIENTMODE_SETTINGS_H

#include <QWidget>

namespace Ui {
class TCPClientMode_settings;
}

class TCPClientMode_settings : public QWidget
{
  Q_OBJECT
  
public:
  explicit TCPClientMode_settings(QWidget *parent = 0);
  ~TCPClientMode_settings();
  
private:
  Ui::TCPClientMode_settings *ui;
};

#endif // TCPCLIENTMODE_SETTINGS_H
