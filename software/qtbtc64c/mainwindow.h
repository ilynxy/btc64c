#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QPlainTextEdit>
#include <QTcpSocket>
#include "COMSettings.h"
#include "TCPSettings.h"
#include "modulesdataview.h"
#include "debugconsole.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
  Q_OBJECT
  
public:
  explicit MainWindow(QWidget *parent = 0);
  ~MainWindow();
  
protected:
  enum
  {
    stDisconnected,
    stConnecting,
    stConnected,
    stDisconnecting
  } state;

  void closeEvent(QCloseEvent *);

  void saveSettings();
  void loadSettings();

  COMSettings comParams;
  TCPSettings tcpParams;

  QSerialPort sp;
  QTcpSocket  ss;

  DebugConsole   *dc;
  QPlainTextEdit *logView;
  modulesdataview *dataView;

  void updateConnectionCB();

  void open_channel();
  void abort_channel();
  void disconnect_channel();
protected slots:
  void readyRead();

  void bytesWritten(qint64 bytes);
  void sendData(const QByteArray &data);

  void connected();
  void disconnected();
  void error(QString error);

private slots:
  void on_connectButton_clicked();
  void on_settingsButton_clicked();

  void socket_error(QAbstractSocket::SocketError);
private:
  Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
