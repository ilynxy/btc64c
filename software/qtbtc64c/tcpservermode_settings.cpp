#include "tcpservermode_settings.h"
#include "ui_tcpservermode_settings.h"

tcpservermode_settings::tcpservermode_settings(TCPSettings& ts_, QWidget *parent) :
  QDialog(parent),
  ui(new Ui::tcpservermode_settings),
  ts(ts_)
{
  ui->setupUi(this);

  connect(this, SIGNAL(accepted()), this, SLOT(settings_accepted()));

  ui->leHostName->setText(ts.hostName);
  ui->lePort->setText(QString::number(ts.port));
}

tcpservermode_settings::~tcpservermode_settings()
{
  delete ui;
}

void tcpservermode_settings::settings_accepted()
{
  ts.hostName = ui->leHostName->text();
  ts.port = ui->lePort->text().toUInt();
}
