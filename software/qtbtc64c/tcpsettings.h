#ifndef TCPSETTINGS_H
#define TCPSETTINGS_H

struct TCPSettings
{
public:
  TCPSettings() :
    hostName("192.168.0.206"),
    port(4001)
  {

  }

  ~TCPSettings()
  {

  }

  QString hostName;
  quint16 port;
};

#endif // TCPSETTINGS_H
