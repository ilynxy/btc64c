#ifndef COMSETTINGS_H
#define COMSETTINGS_H

#include <QtSerialPort/QSerialPort>
#include <QtSerialPort/QSerialPortInfo>

struct COMSettings
{
public:
  COMSettings() :
    portName    ("COM1"),
    baudRate    (921600),
    dataBits    (QSerialPort::Data8),
    parity      (QSerialPort::NoParity),
    stopBits    (QSerialPort::OneStop),
    flowControl (QSerialPort::NoFlowControl)
  {
  }

  ~COMSettings()
  {
  }

  QString                   portName;
  qint32                    baudRate;
  QSerialPort::DataBits     dataBits;
  QSerialPort::Parity       parity;
  QSerialPort::StopBits     stopBits;
  QSerialPort::FlowControl  flowControl;
};


Q_DECLARE_METATYPE(QSerialPort::DataBits)
Q_DECLARE_METATYPE(QSerialPort::Parity)
Q_DECLARE_METATYPE(QSerialPort::StopBits)
Q_DECLARE_METATYPE(QSerialPort::FlowControl)

#endif // COMSETTINGS_H
