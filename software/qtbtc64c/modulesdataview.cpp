#include "modulesdataview.h"
#include "ui_modulesdataview.h"


modulesdataview::modulesdataview(QWidget *parent) :
  QWidget(parent),
  ui(new Ui::modulesdataview)
{
  ui->setupUi(this);
  model = new dataviewmodel(this);
  ui->treeView->setModel(model);

  frame.clear();
  rfcs = 0;
}

modulesdataview::~modulesdataview()
{
  delete ui;
}

void modulesdataview::parseData(const QByteArray& data)
{
  int size = data.size();
  for (int i = 0; i < size; ++ i)
  {
    quint8 b;
    b = data.at(i);

    if (frame.size() == 17 && rfcs == b)
    {
      dataviewmodel::channelData r;
      r.channel = frame.at(0);
      for (int k = 0; k < 8; ++ k)
      {
        r.v[k] = 256UL * quint16(frame.at(2 * k + 1)) + frame.at(2 * k + 2);
      }

      model->setChannelData(r);

      frame.clear();
      rfcs = 0;
    }
    else
    {
      rfcs += b;
      frame.push_back(b);
      while (frame.size() > 17)
      {
        quint8 s = frame.at(0);
        rfcs -= s;
        frame.pop_front();
      }
    }
  }
}

void modulesdataview::on_pushButton_2_clicked()
{
  dataviewmodel::channelData r;
  r.channel = rand() % 16;
  for (int i = 0; i < 8; ++ i)
  {
    quint16 u = rand() + rand();
    r.v[i] = u;
  }

  model->setChannelData(r);
}

void modulesdataview::on_pushButton_clicked()
{
  model->resetChannelData();
}

void modulesdataview::on_pushButton_3_clicked()
{
    model->updateChannelData();
}
