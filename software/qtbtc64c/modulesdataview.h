#ifndef MODULESDATAVIEW_H
#define MODULESDATAVIEW_H

#include <QWidget>
#include "dataviewmodel.h"
#include "QVector"

namespace Ui {
class modulesdataview;
}

class modulesdataview : public QWidget
{
  Q_OBJECT
  
public:
  explicit modulesdataview(QWidget *parent = 0);
  ~modulesdataview();
  
  void parseData(const QByteArray& data);

private slots:
  void on_pushButton_2_clicked();

  void on_pushButton_clicked();

  void on_pushButton_3_clicked();

private:
  Ui::modulesdataview *ui;

  dataviewmodel* model;

  QVector<quint8>    frame;
  quint8             rfcs;
};

#endif // MODULESDATAVIEW_H
