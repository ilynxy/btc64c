#include "commandlineeditor.h"

#include <QLineEdit>
#include <QValidator>

/*
class CmdLineValidator : public QValidator
{
  Q_OBJECT
public:
  explicit CmdLineValidator(QObject *parent = 0);
  virtual State validate (QString &input, int &pos) const
  {
    return Acceptable;
  }
};
*/

bool ParseCmdLine(const QString& s, QByteArray& d)
{
  bool valid = true;

  int cp;
  cp = s.lastIndexOf(QChar(':'));
  ++ cp;

  QString sv;
  char pcs = 0;

  for (int i = cp; i < s.length(); ++ i)
  {
    if (sv.length() != 0 || i == (s.length() - 1) )
    {
      char b;
      bool ok;

      if (!s[i].isSpace())
        sv.append(s[i]);

      if (sv != QChar('$'))
      {
        b = static_cast<char>(sv.toUInt(&ok, 16));

        valid &= ok;

        if (ok)
        {
          d.append(b);
          pcs += b;
        }
      }
      else
      {
        d.append(pcs);
        pcs = 0;
      }
      sv.clear();
    }
    else
    {
      if (!s[i].isSpace())
        sv.append(s[i]);
    }

  }

  return valid;
}

CommandLineEditor::CommandLineEditor(QWidget *parent) :
  QComboBox(parent)
{
  setEditable(true);
  connect(lineEdit(), SIGNAL(textChanged(const QString&)), this, SLOT(on_text_changed(const QString&)));
  connect(lineEdit(), SIGNAL(returnPressed()), this, SLOT(on_returnPressed()));
}

void CommandLineEditor::on_text_changed(const QString& s)
{
/*
  QByteArray d;
  ParseCmdLine(s, d);

  qDebug() << d;
*/
}

void CommandLineEditor::on_returnPressed()
{
  QByteArray d;
  QString s = currentText();
  ParseCmdLine(s, d);

  if (d.size() != 0)
  {
    emit sendData(d);
  }

  clearEditText();
}
