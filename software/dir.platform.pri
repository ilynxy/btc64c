unix {
    _OUTDIRPFX = "../.bin/"
    _INTDIRPFX = "../.bin/.cache/"

    CONFIG(debug, debug|release) {
        _CONFIGURATION = "debug"
    } else {
        _CONFIGURATION = "release"
    }

#    !contains(QMAKE_TARGET.arch, x86_64) {
#        _PLATFORM = "x86"
#    } else {
#        _PLATFORM = "x86_64"
#    }

    _PLATFORM = $${QMAKE_TARGET.arch}

    _OUTDIR = $${_OUTDIRPFX}$${_CONFIGURATION}.$${_PLATFORM}
    _INTDIR = $${_INTDIRPFX}$${TARGET}/$${_CONFIGURATION}.$${_PLATFORM}

    DESTDIR = $${_OUTDIR}

    OBJECTS_DIR = $${_INTDIR}
    MOC_DIR = $${_INTDIR}
    UI_DIR  = $${_INTDIR}
    RCC_DIR = $${_INTDIR}
}

win32 {
    _OUTDIRPFX = "../.bin/"
    _INTDIRPFX = "../.bin/.cache/"
    
    CONFIG(debug, debug|release) {
        _CONFIGURATION = "Debug"
    } else {
        _CONFIGURATION = "Release"
    }
    

    message(QMAKE_TARGET.arch: $${QMAKE_TARGET.arch})
    message(QT_VERSION: $${QT_VERSION})

    !contains(QMAKE_TARGET.arch, x86_64) {
        _PLATFORM = "Win32"
    } else {
        _PLATFORM = "x64"
    }

    _OUTDIR = $${_OUTDIRPFX}$${_CONFIGURATION}.$${_PLATFORM}
    _INTDIR = $${_INTDIRPFX}$${TARGET}/$${_CONFIGURATION}.$${_PLATFORM}

    CONFIG(debug, debug|release) {
        _PDB = $${_INTDIR}/$${TARGET}.pdb

        QMAKE_CFLAGS_DEBUG   += /Fd$${_PDB}
        QMAKE_CXXFLAGS_DEBUG += /Fd$${_PDB}

        QMAKE_CLEAN += $$replace(_PDB, /, \\)

        CONFIG -= embed_manifest_exe
    }

    message(_OUTDIR: $${_OUTDIR})
    message(_INTDIR: $${_INTDIR})

    DESTDIR = $${_OUTDIR}

    OBJECTS_DIR = $${_INTDIR}
    MOC_DIR = $${_INTDIR}
    UI_DIR  = $${_INTDIR}
    RCC_DIR = $${_INTDIR}
    PRECOMPILED_DIR = $${_INTDIR}
}
